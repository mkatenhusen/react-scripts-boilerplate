// Request wrapper
import * as axios from 'axios';
import i18n from "../i18n";

function request(options) {
  const requestOptions = options;


  // if no method in options - then try 'get' by default
  if (options.method === undefined) {
    requestOptions.method = 'get';
  }

  const onSuccess = function (response) {
    return response.data;
  };

  // disabled console.log for production is possible
  const onError = function (error) {
    // console.error('Request Failed:', error.config);

    if (error.response) {
      // Request was made but server responded with something
      // other than 2xx
      // console.error('Status:', error.response.status);
      // console.error('Data:', error.response.data);
      // console.error('Headers:', error.response.headers);
    } else {
      // Something else happened while setting up the request
      // triggered the error
      // console.error('Error Message:', error.message);
    }

    return Promise.reject(error.response || error.message);
  };

  const client = axios.create({
    baseURL: process.env.REACT_APP_BACKEND_URL,
    withCredentials: true,
    xsrfCookieName: 'csrftoken',
    xsrfHeaderName: 'X-CSRFToken',
    headers: {
      'accept-language': i18n.language,
    }
  });

  return client(requestOptions)
    .then(onSuccess)
    .catch(onError);
}

export default request;
