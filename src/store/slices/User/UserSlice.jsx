import {createSlice} from "@reduxjs/toolkit";

const initialState = {
    isLoggedIn: false,
    isLoading: false,
    hasErrored: false,
    token: null // TODO implement the token for the future
};

const userSlice = createSlice({
    name: 'user',
    initialState,
    reducers: {
        isLoggedInSuccess(state, action) {
            state.isLoading = false;
            state.isLoggedIn = true;
        },
        loggingOutSuccess(state, action) {
            state.isLoading = false;
            state.isLoggedIn = false;
        },
        isLoading(state, action) {
            state.isLoading = true;
            state.hasErrored = false;
        },
        hasErrored(state, action) {
            state.isLoading = false;
            state.hasErrored = true;
        },
    }
});

export default userSlice