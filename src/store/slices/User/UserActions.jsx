import request from "../../../common/request";
import userDetail from "./UserSlice";

/**
 * Calls backend with a post request. A cookie is set (from the backend) in order to authenticate all future requests.
 * We need this slice in order to check if the user can access protected routes.
 * @param username {string}
 * @param password {string}
 */
export function logIn(username, password) {
    return dispatch => {
        dispatch(userDetail.actions.isLoading());
        return request({
            url: `auth/login/`,
            method: 'post',
            data: {
                username: username,
                password: password
            }
        })
            .then((response) => {
                dispatch(userDetail.actions.isLoggedInSuccess(response));
            })
            .catch((error) => {
                dispatch(userDetail.actions.hasErrored())
            });
    };
}

/**
 * Calls the backend with a get request in order to delete the cookie (previously set by the backend).
 * Also, the frontend attribute isLoggedIn is set to false in order to deny the user opening protected routes
 */
export function logOut() {
    return dispatch => {
        dispatch(userDetail.actions.isLoading());
        return request({
            url: `auth/logout/`,
            method: 'get',
        })
            .then((response) => {
                dispatch(userDetail.actions.loggingOutSuccess(response));
            })
            .catch((error) => {
                dispatch(userDetail.actions.hasErrored())
            });
    };
}