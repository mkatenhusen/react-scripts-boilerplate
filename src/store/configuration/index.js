import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage'; // defaults to localStorage for web
import { CookieStorage } from 'redux-persist-cookie-storage';
import Cookies from "cookies-js";
import {combineReducers, configureStore} from '@reduxjs/toolkit';
import thunk from 'redux-thunk';
import { createBrowserHistory } from 'history';
import { routerMiddleware, connectRouter } from 'connected-react-router';

import userSlice from "../slices/User/UserSlice";

const persistRootReducerConfig = {
    key: 'root',
    storage,
    whitelist: ['user'] // you can blacklist as well
};

const persistAuthorizationConfig = {
    key: 'token', // token as this is nested
    storage: new CookieStorage(Cookies), // you can set other stores as well, e.g. LocalStorage
    whitelist: ['authorization']
};


const rootReducer = (history) => {
    return combineReducers({
        router: connectRouter(history),
        //user: persistReducer(persistAuthorizationConfig, userSlice.reducer),
        // see above how to make a part of the store persistent
        user: userSlice.reducer
    });
};

export const history = createBrowserHistory();

const store = configureStore({
    reducer: persistReducer(persistRootReducerConfig, rootReducer(history)),
    middleware: [thunk, routerMiddleware(history)]
});

const persistor = persistStore(store);

export { store, persistor };