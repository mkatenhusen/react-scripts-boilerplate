import React from 'react';
import './App.css';
import Routing from "./navigation/Routing";
import {I18nextProvider} from "react-i18next";
import {Provider} from "react-redux";
import {BrowserRouter} from "react-router-dom";
import { ConnectedRouter } from 'connected-react-router';
import { store, persistor, history } from './store/configuration';
import { PersistGate } from 'redux-persist/integration/react';
import i18n from "./i18n";

function App() {
  return (
      <I18nextProvider i18n={i18n}>
        <Provider store={store}>
          <ConnectedRouter history={history}>
            <PersistGate loading={null} persistor={persistor}>
              <BrowserRouter>
                <Routing />
              </BrowserRouter>
            </PersistGate>
          </ConnectedRouter>
        </Provider>
      </I18nextProvider>
  );
}

export default App;
