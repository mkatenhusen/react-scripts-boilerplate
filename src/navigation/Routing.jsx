import React from 'react';
import {Route, Switch} from "react-router-dom";
import {connect} from "react-redux";
import ProtectedRoute from "./ProtectedRoute";
import Login from "../pages/Login/Login";
import Dashboard from "../pages/Dashboard/Dashboard";

function Routing(props) {
    return (
        <Switch>
            <ProtectedRoute exact path="/" component={Dashboard}/>
            <Route exact path="/login" component={Login}/>
            {/*<ProtectedRoute exact path="/item/:uuid" component={}/>*/}
        </Switch>
    );
}

const mapStateToProps = (state) => {
    return state;
};

export default connect(mapStateToProps)(Routing);