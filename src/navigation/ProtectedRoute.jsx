import { connect } from 'react-redux';
import React from 'react';
import { Redirect, Route } from 'react-router-dom';

const ProtectedRoute = ({ component, ...options}) => {
    const { user } = options;
    // user is logged in
    if (user.isLoggedIn) {
        return (
            <Route
                {...options}
                component={component}
            />
        );
    }

    // user is not logged in
    return (
        <Route
            {...options}
            render={(props) => (
                <Redirect to={{ pathname: "/login", state: { from: props.location } }} />
            )}
        />
    );
};

function mapStateToProps(state) {
    return state;
}

export default connect(mapStateToProps)(ProtectedRoute);
