import {logIn} from "../../store/slices/User/UserActions";
import {compose} from "redux";
import {withTranslation} from "react-i18next";
import React from "react";
import {connect} from "react-redux";

function Login(props) {

    return (
        <div>
            <h1>
                Some Login page
            </h1>
           you can add translations with the props.t() function, e.g. {props.t("translation")} - the language is auto detected using the browsers default <br/>
            <button onClick={() => props.submitLogin("my_username", "test123")}>
                Log me in
            </button>
        </div>
    )
}


const mapStateToProps = (state) => {
    return state
};

const mapDispatchToProps = (dispatch) => ({
    submitLogin: (username, password) => dispatch(logIn(username, password)),
});

export default compose(withTranslation(), connect(mapStateToProps, mapDispatchToProps))(Login)